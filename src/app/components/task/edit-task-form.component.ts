/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import { AfterViewChecked, Component, EventEmitter, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';

import { AppEvents } from '../../app.events';
import { EventService } from '../../services/event';
import { Task } from '../../model';

@Component({
  selector: 'edit-task-form',
  templateUrl: './edit-task-form.component.html',
  styleUrls: ['./edit-task-form.component.less']
})
export class EditTaskFormComponent implements AfterViewChecked, OnInit {

  @Input() task: Task;

  private taskForm: FormGroup;
  private loaded: boolean = false;

  constructor(
    private _eventService: EventService,
    private _formBuilder: FormBuilder
  ) {
  }

  ngAfterViewChecked(): void {
    if (!this.loaded && !_.isUndefined(this.task)) {
      this.taskForm.patchValue(this.task);
      this.loaded = true;
    }
  }

  ngOnInit(): void {
    this.taskForm = this._createForm();
  }

  private cancel(): void {
    this.taskForm.reset();
    this._eventService.post(AppEvents.NAV_DASHBOARD_PAGE);
  }

  private submit({ value, valid }: { value: Task, valid: boolean }): void {
    if (valid) {
      this._eventService.post(AppEvents.EDIT_TASK, {
        task: value
      });
      this._eventService.post(AppEvents.NAV_DASHBOARD_PAGE);
    }
  }

  private _createForm(): FormGroup {
    return this._formBuilder.group({
      id:  ['', Validators.required],
      name: ['', Validators.required],
      status: [''],
      description: ['']
    });
  }

}
