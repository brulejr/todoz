/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import {
  ChangeDetectorRef,
  Component,
  ContentChild,
  Input,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import * as _ from 'lodash';

import { Task } from '../../model';

@Component({
  selector: 'lane',
  templateUrl: './lane.component.html',
  styleUrls: ['./lane.component.less']
})
export class LaneComponent implements OnInit, OnDestroy {

  @Input() labelId: string;
  @Input() tasks: Observable<Task[]>;
  @Input() status: string;
  @ContentChild('addCardTitle') addCardTitleTemplate: TemplateRef<Object>;
  @ContentChild('addCardForm') addCardFormTemplate: TemplateRef<Object>;
  @ViewChild('addCardModal') public addCardModal: ModalDirective;

  private filteredTasks: Task[];
  private _taskSubscription: Subscription;

  constructor(private _changeDetectionRef : ChangeDetectorRef) {
  }

  hideAddCardModal(): void {
    this.addCardModal.hide();
  }

  ngOnDestroy(): void {
    if (this._taskSubscription) {
      this._taskSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    this._taskSubscription = this.tasks.subscribe(tasks => {
      this.filteredTasks = tasks.filter(task => task.status === this.status);
      this._changeDetectionRef.detectChanges();
    });
  }

  showAddCardModal(): void {
    this.addCardModal.show();
  }

  private isAddCardEnabled(): boolean {
    return !_.isUndefined(this.addCardTitleTemplate) && !_.isUndefined(this.addCardFormTemplate);
  }

}
