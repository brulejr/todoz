/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import { Injectable, OnDestroy } from '@angular/core';
import { Event, EventService } from '../event';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { UUID } from 'angular2-uuid';
import * as _ from 'lodash';

import { AppEvents } from '../../app.events';
import { Task } from '../../model';

@Injectable()
export class TaskService implements OnDestroy {

  private _createTaskSubscription: Subscription;
  private _taskSubject: BehaviorSubject<Task[]> = new BehaviorSubject([]);
  private _tasks: Task[] = [];

  constructor(private _eventService: EventService) {
    this._createTaskSubscription = this._eventService.on(AppEvents.CREATE_TASK)
      .subscribe(event => this._createTask(event));
    this._createTaskSubscription = this._eventService.on(AppEvents.EDIT_TASK)
      .subscribe(event => this._editTask(event));
  }

  findById(taskId: string): Observable<Task> {
    let task: Task = _.head(_.filter(this._tasks, task => {
      return task.id === taskId;
    }));
    return Observable.of(task);
  }

  ngOnDestroy(): void {
    if (this._createTaskSubscription) {
      this._createTaskSubscription.unsubscribe();
    }
  }

  get tasks(): Observable<Task[]> {
    return new Observable<Task[]>(fn => this._taskSubject.subscribe(fn));
  }

  private _createTask(event: Event): void {
    this._tasks.push(_.merge(event.data.task, {
      id: UUID.UUID(),
      status: "CREATED"
    }));
    this._taskSubject.next(this._tasks);
  }

  private _editTask(event: Event): void {
    let updated: Task = event.data.task;
    let found: Task = _.head(_.filter(this._tasks, task => task.id === updated.id));
    if (!_.isUndefined(found)) {
      found = _.merge(found, {
        description: updated.description,
        name: updated.name,
        status: updated.status
      });
    }
  }

}
