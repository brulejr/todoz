/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { TranslateService } from '@ngx-translate/core';

import { AppEvents } from '../../app.events';
import { EventService } from '../../services/event';
import { Task } from '../../model';
import { TaskService } from '../../services/task';

@Component({
  templateUrl: './task-page.component.html',
  styleUrls: [ './task-page.component.less' ]
})
export class TaskPageComponent implements AfterViewInit, OnInit {

  private task: Task;

  constructor(
    private _changeDetectionRef: ChangeDetectorRef,
    private _eventService: EventService,
    private _route: ActivatedRoute,
    private _taskService: TaskService,
    private _translateService: TranslateService
  ) {
  }

  ngAfterViewInit(): void {
    this._route.params.subscribe((params: Params) => {
      let taskId: string = params['taskId'];
      this._taskService.findById(taskId).subscribe(task => {
        this.task = task;
        this._changeDetectionRef.detectChanges();
      });
    });
  }

  ngOnInit(): void {
    this._eventService.post(AppEvents.APP_TITLE, {name: 'page.task.title'});
  }

}
