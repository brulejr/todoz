/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import { AfterViewInit, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { DOCUMENT, Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core'
import { Subscription } from 'rxjs/Subscription';
import * as _ from 'lodash';

import { AppEvents } from './app.events';
import { EventService } from './services/event';
import { TaskService } from './services/task';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.less' ]
})
export class AppComponent implements AfterViewInit, OnDestroy {

  private _eventHandlers: Subscription[] = [];

  constructor(
    @Inject(DOCUMENT) private _document,
    private _eventService: EventService,
    private _router: Router,
    private _taskService: TaskService,
    private _titleService: Title,
    private _translateService: TranslateService
  ) {
    this._configureI18N();
    this._configureRoutingEvents();
    this._configureTitleHandler();
  }

  ngAfterViewInit(): void {
    //this._translateService.get('appl.copyright').subscribe(version => {
    //  this._document.querySelector('footer > .copyright').innerHTML = version;
    //});
  }

  ngOnInit(): void {
    this._eventService.post(AppEvents.NAV_DASHBOARD_PAGE);
  }

  ngOnDestroy(): void {
    _.forEach(this._eventHandlers, handler => handler.unsubscribe());
  }

  private _configureI18N(): void {
    this._translateService.setDefaultLang('en');
    this._translateService.use('en');
  }

  private _configureRoutingEvents(): void {
    this._registerEventHandler(AppEvents.NAV_DASHBOARD_PAGE, (event) => this._router.navigate(['/dashboard']));
    this._registerEventHandler(AppEvents.NAV_HISTORY_PAGE, (event) => this._router.navigate(['/history']));
    this._registerEventHandler(AppEvents.NAV_TASK_PAGE, (event) => {
      this._router.navigate(['/task', event.data.taskId]);
    });
  }

  private _configureTitleHandler(): void {
    this._registerEventHandler(AppEvents.APP_TITLE, (event) => {
      this._translateService.get(event.data.name, event.data.options).subscribe(title => {
        this._titleService.setTitle(title);
      });
    });
  }

  private _registerEventHandler(topic, handler) {
    this._eventHandlers.push(this._eventService.on(topic).subscribe(handler));
  }

}
