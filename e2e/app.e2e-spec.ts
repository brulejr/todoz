import { TodozPage } from './app.po';

describe('todoz App', () => {
  let page: TodozPage;

  beforeEach(() => {
    page = new TodozPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
